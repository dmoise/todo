$('#new-task').submit(function() {
    var toadd = $('#new-name').val();
    if (toadd==""){
        $('#new-result').html('Nothing to add');
        setInterval( function() {$('#new-result').html('');}, 5000);
        return false;
    }
    else {
        $.post("operatii/insert.php",
            {
                name: toadd
            },
            function(data,status){
                $('#new-result').html( data );
            });
    }
})


$(".fa-trash-o").click(function() {
   var taskid=$(this).parent().attr('id');
    $.post("operatii/delete.php",
        {
                taskid: taskid
        },
        function (data, status) {
                $(taskid).html(data);
        });
    $('#' + taskid).remove();
});

$(".checkbox").change(function() {
    var task = $(this).parent().parent();
    var task_id = task.attr('id');  //to submit
    var checkbox = $(task).find('input');
    if(this.checked) {
        var status=0;
        task.find('.square').addClass('fa-check');
        $(task).addClass('done');
        $(task).removeClass('undone');
    }
    else{
        var status=1;
        $(task).find('.square').removeClass('fa-check');
        $(task).addClass('undone');
        $(task).removeClass('done');
    }
    $.post("operatii/update.php",
            {
                id: task_id,
                status:status
            },
            function (data, status) {
                var last=$('.done:last');
                if(checkbox.prop('checked')){
                    $(task).insertAfter(last);
                }
                else{
                    $(task).insertAfter($('.undone:last'));
                }
            });
});

