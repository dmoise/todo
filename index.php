<! DOCTYPE html>
<html>
<head>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>TO DO LIST</title>
<!--        <link href="resurse/css/bootstrap.min.css" rel="stylesheet">-->
        <link href="resurse/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="resurse/css/sass-custom.css" rel="stylesheet">
    </head>
</head>

<body>
<div class="container">
    <div class="col-lg-7 block clearfix">
        <div class="row">
            <h1 class="text-center text-uppercase">To do list</h1>
            <div class="col-lg-12">
                <form action="" method="POST" id="new-task">
                        <input placeholder="Nume task" value="" name="new-name" id="new-name" class="form-control"/>
                        <button type="submit" class="btn btn-info" id="add">
                            <i class="fa fa-plus"></i>
                        </button>
                        <div id="new-result"> </div>
                </form>
            </div>

            <div class="col-lg-12">
                <?php
                include ('operatii/connect.php');
                $sql = "SELECT * FROM `task`";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        ?>
                        <div class="well <?php if ($row["status"]!=1) echo 'done'; else echo 'undone'; ?>" id="<?php echo $row["id"] ?>">
                            <label for="check-<?php echo $row["id"] ?>" >
                                <i class="square fa <?php if ($row["status"]==0) echo 'fa-check'; ?> text-center" aria-hidden="true"></i>
                                <input type="checkbox" value="" id="check-<?php echo $row["id"] ?>" name="task<?php echo $row["id"] ?>" class="checkbox hidden" <?php if ($row["status"]==0) echo 'checked'; ?>/>
                                <?php echo $row["name"] ?></label>
                            <i class="fa fa-trash-o pull-right"></i>
                        </div>
                        <?php
                     }
                    }
                else {
                    echo "No tasks";
                }
                $conn->close();
                ?>

            </div>
        </div>
    </div>
</div> <!-- /container -->
    <script src="resurse/js/jquery-3.1.1.min.js"></script>
    <script src="resurse/js/custom.js"></script>

</body>



</html>
