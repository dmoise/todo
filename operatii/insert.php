<?php

$name=htmlspecialchars($_POST["name"]);

include ('connect.php');

$sql = "INSERT INTO task (name) VALUES ('$name')";

if ($conn->query($sql) === TRUE) {
    $last_id = $conn->insert_id;
    echo "<div class=\"alert alert-success\" role=\"alert\"> <strong>".$name. "</strong> inserted successfully. Refresh the page</div>";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
